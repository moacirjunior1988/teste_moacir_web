# language: pt
# encoding: iso-8859-1
Funcionalidade: Cadastrar novo usuario

  Contexto:
    Dado que acesso a tela de home do e-commerce

  @CadastrarNovoUsuario
  Cenario: Cadastrar novo usuario
    Dado que estou na tela home
    E acesso o menu
      | Menu   |
      | Signup |
    E informo os campos nome e email para iniciar o cadastro
      | Nome       | Email      |
      | Automatico | Automatico |
    E clico no botao signup
    E informo todos os campos da tela de cadastro de usuario
      | Title      | Name       | Password   | DayOfBirth | MonthOfBirth | YearOfBirth | Newsletter | Receive | FirstName  | LastName   | Company        | Address_1  | Address_2  | Country       | State      | City  | ZipCode | MobileNumber |
      | Automatico | Automatico | Automatico | 25         | April        | 1975        | True       | True    | Automatico | Automatico | Street address | Automatico | Automatico | United States | California | Miami | 33101   | 8002561781   |
    Quando clico no botao create account
    Entao devera apresentar a account created
