# language: pt
# encoding: iso-8859-1
Funcionalidade: Colocar Produtos No Carrinho

  Contexto:
    Dado que acesso a tela de home do e-commerce

  @ColocarProdutosNoCarrinho
  Cenario: Colocar produtos no carrinho
    Dado que estou na tela home
    E acesso o menu
      | Menu     |
      | products |
    E coloco no meu carrinho os produtos com suas quantidades
      | nome_produto_1 | nome_produto_2                            | nome_produto_3 | quantidade_produto_1 | quantidade_produto_2 | quantidade_produto_3 |
      | Stylish Dress  | Beautiful Peacock Blue Cotton Linen Saree | Men Tshirt     | 3                    | 2                    | 1                    |
    E acesso o menu
      | Menu     |
      | viewCart |
    Entao devera apresentar os produtos que foram colocados no carrinho com suas devidas quantidades
      | nome_produto_1 | nome_produto_2                            | nome_produto_3 | quantidade_produto_1 | quantidade_produto_2 | quantidade_produto_3 |
      | Stylish Dress  | Beautiful Peacock Blue Cotton Linen Saree | Men Tshirt     | 3                    | 2                    | 1                    |
