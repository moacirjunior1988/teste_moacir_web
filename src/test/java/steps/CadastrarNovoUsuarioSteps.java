package steps;

import cucumber.api.DataTable;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import geral.extentReport;
import geral.utils;
import pageObject.AccountCreatedPO;
import pageObject.LoginPO;
import pageObject.SignupPO;
import static org.junit.Assert.*;
import java.util.List;
import java.util.Map;

import static geral.utils.*;

public class CadastrarNovoUsuarioSteps
{
    String[] listMonthWith30Days = {"4", "6", "9", "11"};
    @E("^informo os campos nome e email para iniciar o cadastro$")
    public void informoOsCamposNomeEEmailParaIniciarOCadastro(DataTable dataTable)
    {
        extentReport.SalvarLogSucesso("E informo os campos nome e email para iniciar o cadastro");
        List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
        String geral = "";

        switch (data.get(0).get("Nome"))
        {
            case "Automatico":
                geral = utils.GeradorNome();
                extentReport.SalvarLogSucesso("Nome: " + geral);
                break;
            default:
                geral= data.get(0).get("Nome");
                break;
        }
        LoginPO.writeName(geral);

        switch (data.get(0).get("Email"))
        {
            case "Automatico":
                geral = utils.createEmailMailinator();
                break;
            default:
                geral = data.get(0).get("Email");
                break;
        }
        LoginPO.writeEmail(geral);
        extentReport.SalvarLogScreenShotSucesso("Signup preenchido.");
    }


    @E("^clico no botao signup$")
    public void clicoNoBotaoSignup()
    {
        LoginPO.clickSignup();
        Sleep(1000);
        extentReport.SalvarLogScreenShotSucesso("Tela de signup.");
    }

    @E("^informo todos os campos da tela de cadastro de usuario$")
    public void informoTodosOsCamposDaTelaDeCadastroDeUsuario(DataTable dataTable)
    {
        List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
        Integer Day = 0;
        if(data.get(0).get("Title").equals("Automatico"))
        {
            if(RandomNumber(2) == 1) SignupPO.clickTitleMr();
            else SignupPO.clickTitleMrs();
        }
        else
        {
            if(data.get(0).get("Title").toLowerCase().equals("masculino") || data.get(0).get("Title").toLowerCase().equals("homem"))
                SignupPO.clickTitleMr();
            else SignupPO.clickTitleMrs();
        }

        if(data.get(0).get("Password").equals("Automatico"))
        {
            SignupPO.writePassword(GeneratorPasswordAleatory(10));
        }
        else
        {
            SignupPO.writePassword(data.get(0).get("Password"));
        }

        SignupPO.selectDayOfBirth(data.get(0).get("DayOfBirth"));

        SignupPO.selectMonthOrBirth(data.get(0).get("MonthOfBirth"));

        SignupPO.selectYearOrBirth(data.get(0).get("YearOfBirth"));

        if(data.get(0).get("Newsletter").equals("True"))
        {
            SignupPO.clickNewsletter();
        }

        if(data.get(0).get("Receive").equals("True"))
        {
            SignupPO.clickReceive();
        }

        if(data.get(0).get("FirstName").equals("Automatico"))
        {
            SignupPO.writeFirstName(utils.GeradorNome());
        }
        else
        {
            SignupPO.writeFirstName(data.get(0).get("FirstName"));
        }

        if(data.get(0).get("LastName").equals("Automatico"))
        {
            SignupPO.writeLastName(utils.GeradorNome());
        }
        else
        {
            SignupPO.writeLastName(data.get(0).get("LastName"));
        }

        SignupPO.writeCompany(data.get(0).get("Company"));

        if(data.get(0).get("Address_1").equals("Automatico"))
        {
            SignupPO.writeAddress(utils.GeradorNome());
        }
        else
        {
            SignupPO.writeAddress(data.get(0).get("Address_1"));
        }

        if(data.get(0).get("Address_2").equals("Automatico"))
        {
            SignupPO.writeAddress2(utils.GeradorNome());
        }
        else
        {
            SignupPO.writeAddress2(data.get(0).get("Address_2"));
        }

        SignupPO.selectCountry(data.get(0).get("Country"));

        SignupPO.writeState(data.get(0).get("State"));

        SignupPO.writeCity(data.get(0).get("City"));

        SignupPO.writeZipcode(data.get(0).get("ZipCode"));

        SignupPO.writeMobileNumber(data.get(0).get("MobileNumber"));

        Sleep(1000);
        extentReport.SalvarLogScreenShotSucesso("Tela de signup preenchida.");
    }


    @Quando("^clico no botao create account$")
    public void clicoNoBotaoCreateAccount()
    {
        SignupPO.clickCreateAccount();
        Sleep(3000);
    }

    @Entao("^devera apresentar a account created$")
    public void deveraApresentarAAccountCreated()
    {
        assertTrue(AccountCreatedPO.validationOfExibitionOfButtonContine());
        extentReport.SalvarLogScreenShotSucesso("Tela de account created.");
    }
}
