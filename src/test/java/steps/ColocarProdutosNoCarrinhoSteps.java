package steps;

import cucumber.api.DataTable;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import geral.extentReport;
import geral.hooks;
import pageObject.HomePO;
import pageObject.LoginPO;
import pageObject.ProductsPO;
import pageObject.ViewCartPO;
import java.util.List;
import java.util.Map;
import static geral.utils.*;
import static org.junit.Assert.*;

public class ColocarProdutosNoCarrinhoSteps {

    @Dado("^que estou na tela home$")
    public void queEstouNaTelaHome()
    {
        extentReport.SalvarLogScreenShotSucesso("Dado que estou na tela home");
        assertTrue(HomePO.validationOfExibitionOfImageLogo());
    }

    @E("^acesso o menu$")
    public void acessoOMenu(DataTable dataTable)
    {
        List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
        switch (data.get(0).get("Menu"))
        {
            case "products":
                HomePO.clickMenuProducts();
                Sleep(3000);
                assertTrue(ProductsPO.validationCurrentUrl());
                assertTrue(ProductsPO.validationOfExibitionOfImageSale());
                break;

            case "viewCart":
                HomePO.clickMenuViewCart();
                assertTrue(ViewCartPO.validationOfExibitionButtonProceedToCheckout());
                Sleep(3000);
                break;

            case "Signup":
                HomePO.clickMenuSignupLogin();
                assertTrue(LoginPO.validationOfExibitionOfFieldName());
                Sleep(3000);
                break;
        }
        extentReport.SalvarLogScreenShotSucesso("E acesso o menu");
    }

    @E("^coloco no meu carrinho os produtos com suas quantidades$")
    public void colocoNoMeuCarrinhoOsProcutosComSuasQuantidades(DataTable dataTable)
    {
        List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);

        for(int a = 0; a <= 2; a++)
        {
            String nomeProduto = "";
            int quantidadeProduto = 0;

            switch (a)
            {
                case 0:
                    nomeProduto = data.get(0).get("nome_produto_1");
                    break;

                case 1:
                    nomeProduto = data.get(0).get("nome_produto_2");
                    break;

                case 2:
                    nomeProduto = data.get(0).get("nome_produto_3");
                    break;
            }

            ProductsPO.writeSearch(nomeProduto);
            ProductsPO.clickSearch();

            switch (a)
            {
                case 0:
                    quantidadeProduto = Integer.parseInt(data.get(0).get("quantidade_produto_1"));
                    break;

                case 1:
                    quantidadeProduto = Integer.parseInt(data.get(0).get("quantidade_produto_2"));
                    break;

                case 2:
                    quantidadeProduto = Integer.parseInt(data.get(0).get("quantidade_produto_3"));
                    break;
            }

            for(int b = 1; b <= quantidadeProduto; b++)
            {
                ProductsPO.clickFocoImageProduct();
                Sleep(1000);
                ProductsPO.clickAddToCart();
                Sleep(1000);
                ProductsPO.clickContinueShopping();
                Sleep(1000);
            }
        }
        extentReport.SalvarLogScreenShotSucesso("E coloco no meu carrinho os produtos com suas quantidades");
    }

    @Entao("^devera apresentar os produtos que foram colocados no carrinho com suas devidas quantidades$")
    public void deveraApresentarOsProdutosQueForamColocadosNoCarrinhoComSuasDevidasQuantidades(DataTable dataTable)
    {
        extentReport.SalvarLogScreenShotSucesso("Então deverá apresentar os produtos que foram colocados no carrinho com suas devidas quantidades");
        List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
        List<String> listInformationProductsLine = null;

        for (int a = 1; a <= ViewCartPO.returnAmountLinesFromProducts(); a++)
        {
            listInformationProductsLine = ViewCartPO.retornListValueFromProductsInViewCart(a);
            String nomeProduto = "", quantidadeProduto = "";

            switch (a)
            {
                case 1:
                    nomeProduto = data.get(0).get("nome_produto_1");
                    quantidadeProduto = data.get(0).get("quantidade_produto_1");
                    break;

                case 2:
                    nomeProduto = data.get(0).get("nome_produto_2");
                    quantidadeProduto = data.get(0).get("quantidade_produto_2");
                    break;

                case 3:
                    nomeProduto = data.get(0).get("nome_produto_3");
                    quantidadeProduto = data.get(0).get("quantidade_produto_3");
                    break;
            }
            assertEquals(nomeProduto, listInformationProductsLine.get(0));
            extentReport.SalvarLogSucesso(nomeProduto + " = " + listInformationProductsLine.get(0));
            assertNotEquals(listInformationProductsLine.get(1), "");
            extentReport.SalvarLogSucesso(listInformationProductsLine.get(1) + " != " + "");
            assertEquals(quantidadeProduto, listInformationProductsLine.get(2));
            extentReport.SalvarLogSucesso(quantidadeProduto + " = " + listInformationProductsLine.get(2));
            assertNotEquals(listInformationProductsLine.get(3), "");
            extentReport.SalvarLogSucesso(listInformationProductsLine.get(3) + " != " + "");
            hooks.ultimoScenario = true;
        }
    }
}
