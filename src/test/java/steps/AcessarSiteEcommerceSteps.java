package steps;

import com.aventstack.extentreports.ExtentTest;
import cucumber.api.java.pt.Dado;
import geral.extentReport;
import geral.seleniumActions;
import geral.testConfiguration;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AcessarSiteEcommerceSteps
{
    protected ExtentTest extentTest;
    private geral.extentReport extentReport = new extentReport();
    public AcessarSiteEcommerceSteps()
    {
        this.extentTest = geral.extentReport.test;
    }

    @Dado("que acesso a tela de home do e-commerce$")
    public void dadoQueAcessoATelaDeHomeDoEcommerce()
    {
        extentReport.SalvarLogSucesso("Dado que acesso a tela de home do e-commerce.");

        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        options.setHeadless(false);
        seleniumActions.webDriver = new ChromeDriver();
        seleniumActions.wait = new WebDriverWait(seleniumActions.webDriver, 3000);
        seleniumActions.webDriver.navigate().to(testConfiguration.url);
        seleniumActions.webDriver.manage().window().maximize();

        geral.extentReport.SalvarLogScreenShotSucesso("tela de home exibida.");
    }
}
