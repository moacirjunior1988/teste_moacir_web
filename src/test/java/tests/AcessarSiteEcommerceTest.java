package tests;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/java/features/AcessarSiteEcommerce.feature",
        tags = {"@AcessarSiteDoECommerce"},
        glue = { "" },
        monochrome = true,
        dryRun = false,
        snippets = SnippetType.CAMELCASE)
public class AcessarSiteEcommerceTest {
}
