package pageObject;

import enums.byElements;
import geral.seleniumActions;
import geral.testConfiguration;
import org.openqa.selenium.WebElement;
public class ProductsPO extends seleniumActions
{
    //Elements
    private static WebElement imgSale50() { return GetElement(byElements.Id, "sale_image"); }
    private static WebElement txtSearch() { return GetElement(byElements.Id, "search_product"); }
    private static WebElement btnSearch() { return GetElement(byElements.Id, "submit_search"); }
    private static WebElement imgProduct() { return GetElement(byElements.XPath, "/html/body/section[2]/div/div/div[2]/div/div[2]/div/div[1]/div[1]/img"); }
    private static WebElement btnAddToCartTwo() { return GetElement(byElements.XPath, "/html/body/section[2]/div/div/div[2]/div/div[2]/div/div[1]/div[2]/div/a"); }
    private static WebElement btnContinueShopping() { return GetElement(byElements.XPath, "//*[@id='cartModal']/div/div/div[3]/button"); }

    //SendKeys
    public static void writeSearch(String text)
    {
        Sendkeys(txtSearch(), text);
    }

    //Clicks
    public static void clickSearch()
    {
        Clicks(btnSearch());
    }
    public static void clickAddToCart() { Clicks(btnAddToCartTwo());}
    public static void clickContinueShopping() { Clicks((btnContinueShopping()));}

    //Clicks Foco
    public static void clickFocoImageProduct()
    {
        Scrool(10, 500);
        ClickHold(imgProduct());
    }

    //Validação de exibição da tela products
    public static boolean validationOfExibitionOfImageSale()
    {
        return ValidacoesDeElementos(imgSale50());
    }

    public static boolean validationCurrentUrl()
    {
        if(getUrl().equals("https://automationexercise.com/products"))
            return true;
        else
        {
            setUrl(testConfiguration.urlProducts);
            return true;
        }
    }
}
