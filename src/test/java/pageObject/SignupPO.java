package pageObject;

import enums.byElements;
import geral.seleniumActions;
import org.openqa.selenium.WebElement;
import sun.awt.windows.WBufferStrategy;

public class SignupPO extends seleniumActions
{
    //Elements
    private static WebElement rdbTitleMr() { return GetElement(byElements.Id, "id_gender1"); }
    private static WebElement rdbTitleMrs() { return GetElement(byElements.Id, "id_gender2"); }
    private static WebElement txtName() { return GetElement(byElements.Id, "name"); }
    private static WebElement txtEmail() { return GetElement(byElements.Id, "email"); }
    private static WebElement txtPassword() { return GetElement(byElements.Id, "password"); }
    private static WebElement cbbDayOfBirth() { return GetElement(byElements.Id, "days"); }
    private static WebElement cbbMonthOrBirth() { return GetElement(byElements.Id, "months"); }
    private static WebElement cbbYearOrBirth() { return GetElement(byElements.Id, "years"); }
    private static WebElement ckbNewsletter() { return GetElement(byElements.Id, "newsletter"); }
    private static WebElement ckbReceive() { return GetElement(byElements.Id, "optin"); }
    private static WebElement txtFirstName() { return GetElement(byElements.Id, "first_name"); }
    private static WebElement txtLastName() { return GetElement(byElements.Id, "last_name"); }
    private static WebElement txtCompany() { return GetElement(byElements.Id, "company"); }
    private static WebElement txtAddress() { return GetElement(byElements.Id, "address1"); }
    private static WebElement txtAddress2() { return GetElement(byElements.Id, "address2"); }
    private static WebElement cbbCountry () { return GetElement(byElements.Id, "country"); }
    private static WebElement txtState() { return GetElement(byElements.Id, "state"); }
    private static WebElement txtCity() { return GetElement(byElements.Id, "city"); }
    private static WebElement txtZipcode() { return GetElement(byElements.Id, "zipcode"); }
    private static WebElement txtMobileNumber() { return GetElement(byElements.Id, "mobile_number"); }
    private static WebElement btnCreateAccount() { return GetElement(byElements.CssSelector, "#form > div > div > div > div.login-form > form > button"); }

    //Sendkeys
    public static void writeName(String text) { Sendkeys(txtName(), text); }
    public static void writeEmail(String text) { Sendkeys(txtEmail(), text); }
    public static void writePassword(String text) { Sendkeys(txtPassword(), text); }
    public static void writeFirstName(String text) { Sendkeys(txtFirstName(), text); }
    public static void writeLastName(String text) { Sendkeys(txtLastName(), text); }
    public static void writeCompany(String text) { Sendkeys(txtCompany(), text); }
    public static void writeAddress(String text) { Sendkeys(txtAddress(), text); }
    public static void writeAddress2(String text) { Sendkeys(txtAddress2(), text); }
    public static void writeState(String text) { Sendkeys(txtState(), text); }
    public static void writeCity(String text) { Sendkeys(txtCity(), text); }
    public static void writeZipcode(String text) { Sendkeys(txtZipcode(), text); }
    public static void writeMobileNumber(String text) { Sendkeys(txtMobileNumber(), text); }

    //Clicks
    public static void clickCreateAccount() { Clicks(btnCreateAccount()); }

    //ComboBox
    public static void selectCountry(String text) {  ComboBoxSelect(cbbCountry(), text); }
    public static void selectDayOfBirth(String text) {  ComboBoxSelect(cbbDayOfBirth(), text); }
    public static void selectMonthOrBirth(String text) {  ComboBoxSelectValue(cbbMonthOrBirth(), text); }
    public static void selectYearOrBirth(String text) {  ComboBoxSelect(cbbYearOrBirth(), text); }

    //RadioButton
    public static void clickTitleMr() { Clicks(rdbTitleMr()); }
    public static void clickTitleMrs() { Clicks(rdbTitleMrs()); }

    //CheckBox
    public static void clickNewsletter() { ClicksJS(ckbNewsletter()); }
    public static void clickReceive() { ClicksJS(ckbReceive()); }

    //Validation
    public static boolean validationOfExibitionOfFieldTitle() { return ValidacoesDeElementos(rdbTitleMr()); }
}
