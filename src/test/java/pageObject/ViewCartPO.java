package pageObject;

import enums.byElements;
import geral.seleniumActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

public class ViewCartPO extends seleniumActions
{
    //Elements
    private static WebElement btnProceedToCheckout() { return GetElement(byElements.XPath, "//*[@id='do_action']/div[1]/div/div/a"); }
    private static List<WebElement> tblLine() { return GetElements(byElements.XPath, "//*[@id='cart_info_table']/tbody/tr");}

    //Validação de exibição da botão Proceed To Checkout
    public static boolean validationOfExibitionButtonProceedToCheckout()
    {
        return ValidacoesDeElementos(btnProceedToCheckout());
    }

    //Retorno Quantidade de Linhas de produtos
    public static int returnAmountLinesFromProducts()
    {
        Scrool(10, 200);
        return tblLine().size();
    }

    public static List<String> retornListValueFromProductsInViewCart(int numeric)
    {
        List<String> listInformationProductsLine = new ArrayList<String>();;
        for (int a = numeric; a <= numeric; a++)
        {
            listInformationProductsLine.add(webDriver.findElement(By.xpath("//*[@id='cart_info_table']/tbody/tr[" + numeric +"]/td[2]/h4/a")).getText());
            listInformationProductsLine.add(webDriver.findElement(By.xpath("//*[@id='cart_info_table']/tbody/tr[" + numeric + "]/td[3]/p")).getText().toString());
            listInformationProductsLine.add(webDriver.findElement(By.xpath("//*[@id='cart_info_table']/tbody/tr[" + numeric + "]/td[4]/button")).getText().toString());
            listInformationProductsLine.add(webDriver.findElement(By.xpath("//*[@id='cart_info_table']/tbody/tr[" + numeric + "]/td[5]/p")).getText().toString());
        }
        return listInformationProductsLine;
    }
}
