package pageObject;

import enums.byElements;
import geral.seleniumActions;
import org.openqa.selenium.WebElement;

public class HomePO extends seleniumActions
{
    //Elements
    private static WebElement imgHome() { return GetElement(byElements.XPath, "//*[@id='header']/div/div/div/div[1]/div/a/img"); }
    private static WebElement menuProducts() { return GetElement(byElements.XPath, "//*[@id='header']/div/div/div/div[2]/div/ul/li[2]/a"); }
    private static WebElement menuViewCart() { return GetElement(byElements.XPath, "//*[@id='header']/div/div/div/div[2]/div/ul/li[3]/a"); }
    private static WebElement menuSignupLogin() { return GetElement(byElements.XPath, "//*[@id='header']/div/div/div/div[2]/div/ul/li[4]/a");}
    //Clicks
    public static void clickMenuProducts() { Clicks(menuProducts());}
    public static void clickMenuViewCart() { Clicks(menuViewCart()); }
    public static void clickMenuSignupLogin() { Clicks(menuSignupLogin());}

    //Validação de exibição da tela home
    public static boolean validationOfExibitionOfImageLogo()
    {
        return ValidacoesDeElementos(imgHome());
    }
}
