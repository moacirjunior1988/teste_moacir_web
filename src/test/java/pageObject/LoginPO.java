package pageObject;

import enums.byElements;
import geral.seleniumActions;
import org.openqa.selenium.WebElement;

public class LoginPO extends seleniumActions
{
    //Elements
    private static WebElement txtName() { return GetElement(byElements.Name, "name"); }
    private static WebElement txtEmail() { return GetElement(byElements.Name, "email"); }
    private static WebElement btnSignup() { return GetElement(byElements.CssSelector, "#form > div > div > div:nth-child(3) > div > form > button"); }

    //SendKeys
    public static void writeName(String text) { Sendkeys(txtName(), text);}
    public static void writeEmail(String text) { Sendkeys(txtEmail(), text);}

    //Clicks
    public static void clickSignup() { Clicks(btnSignup());}

    //Validation
    public static boolean validationOfExibitionOfFieldName()
    {
        return ValidacoesDeElementos(txtName());
    }
}
