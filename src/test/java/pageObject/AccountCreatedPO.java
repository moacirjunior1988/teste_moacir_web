package pageObject;

import enums.byElements;
import geral.seleniumActions;
import org.openqa.selenium.WebElement;

public class AccountCreatedPO extends seleniumActions
{
    //Elements
    private static WebElement btnContinue() { return GetElement(byElements.XPath, "//*[@id='form']/div/div/div/div/a"); }

    //Validação de exibição da tela home
    public static boolean validationOfExibitionOfButtonContine()
    {
        return ValidacoesDeElementos(btnContinue());
    }
}
