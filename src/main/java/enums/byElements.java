package enums;

public enum byElements
{
    ClassName(0),
    CssSelector(1),
    Id(2),
    LinkText(3),
    Name(4),
    PartialLinkText(5),
    TagName(6),
    XPath(7);

    public int ByElements;

    byElements(int valor)
    {
        ByElements = valor;
    }

}
