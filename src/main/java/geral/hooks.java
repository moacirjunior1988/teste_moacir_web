package geral;

import com.aventstack.extentreports.Status;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;


public class hooks
{
    public static String cenario;
    utils utils = new utils();
    public static Status status;
    public static boolean ultimoScenario = false;
    private static int contadorExtentReports = 0;

    @Before
    public void beforeCenario(Scenario scenario)
    {
        cenario = scenario.getName();
        status = Status.FAIL;

        if(contadorExtentReports == 0)
        {
            extentReport.criarArquivoHTML();
            contadorExtentReports += 1;
        }
        else contadorExtentReports += 1;

        extentReport.test = extentReport.extentReporter.createTest(scenario.getName());
    }

    @After
    public void afterCenario(Scenario scenario)
    {
        if(status == Status.FAIL)
        {
            extentReport.SalvarLogScreenShotFalha("Ocorreu uma falha durante a execução.");
            extentReport.extentReporter.flush();
            seleniumActions.webDriver.quit();
        }
        else
        {
            extentReport.SalvarLogScreenShotSucesso("Execução realizada com sucesso.");

            if(ultimoScenario)
            {
                extentReport.extentReporter.flush();
            }

            seleniumActions.webDriver.quit();
        }
    }

}
