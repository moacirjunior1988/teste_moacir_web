package geral;

import com.aventstack.extentreports.ExtentTest;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.Properties;
import java.util.Random;


public class utils
{
    protected ExtentTest extentTest;
    protected static Properties props = new Properties();
    public static String pathProperties = System.getProperty("user.dir") + "/src/test/resources/configurations.properties";
    public static String pathSalvarEvidencia = Paths.get(System.getProperty("user.dir"), "extentReports").toString();
    private static LocalDateTime dataAtual;
    private static Random random = new Random();
    private static Gson gson = new Gson();
    private static StringBuffer sb;

    public utils()
    {
        this.extentTest = extentReport.test;
    }

    public static String getProp(String var)
    {
        try
        {
            props.load(new FileInputStream(pathProperties));
            return props.getProperty(var);
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static Boolean validationPath(String path) {
        if(Files.exists(Paths.get(path)))
        {
            return true;
        }
        else
        {
            try {
                Files.createDirectories(Paths.get(path));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            return true;
        }
    }

    public boolean verificaMensagem(String msg)
    {
        boolean ok = false;
        if ("".equals(msg)) {
            ok = true;
            extentTest.pass("Mensagem de retorno da requisiÃ§Ã£o [" + msg + "] validada com sucesso!");
        } else {
            extentTest.fail("Mensagem de retorno da requisiÃ§Ã£o [" + msg + "] inexistente!");
        }

        return ok;
    }

    public static void Sleep(long timeOut)
    {
        try
        {
            Thread.sleep(timeOut);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }

    //Method de finalizar os processos do navegador
    public boolean ExecutarCMD(String processo)
    {
        try {
            String line;
            Process p = Runtime.getRuntime().exec("taskkill /im chromedriver.exe /f /t");
            BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
            while ((line = input.readLine()) != null) {
                if (!line.trim().equals("")) {
                    if (line.substring(1, line.indexOf("\"", 1)).equalsIgnoreCase(processo)) {
                        Runtime.getRuntime().exec("taskkill /F /IM " + line.substring(1, line.indexOf("\"", 1)));
                        return true;
                    }
                }
            }
            input.close();
        } catch (Exception err) {
            err.printStackTrace();
        }
        return false;
    }

    public static String diaAtual()
    {
        String  dia = "";
        dataAtual = LocalDateTime.now();

        if(dataAtual.getDayOfMonth() < 10)
        {
            dia = "0" + dataAtual.getDayOfMonth();
        }
        else
        {
            dia = Integer.toString(dataAtual.getDayOfMonth());
        }

        return dia;
    }

    public static String mesAtual()
    {
        String  mes = "";
        dataAtual = LocalDateTime.now();
        int mesAtual = dataAtual.getMonth().getValue();

        if(mesAtual < 10)
        {
            mes = "0" + mesAtual;
        }
        else
        {
            mes = Integer.toString(mesAtual);
        }

        return mes;
    }

    public static String anoAtual()
    {
        dataAtual = LocalDateTime.now();
        return Integer.toString(dataAtual.getYear());
    }

    public static String anosAnterioresAtual(int anosAnteriores)
    {
        dataAtual = LocalDateTime.now();
        return Integer.toString(dataAtual.plusYears(- anosAnteriores).getYear());
    }

    public static String horaAtual()
    {
        String hora = "";
        dataAtual = LocalDateTime.now();

        if(dataAtual.getHour() < 10)
        {
            hora = "0" + Integer.toString(dataAtual.getHour());
        }
        else
        {
            hora = Integer.toString(dataAtual.getHour());
        }

        return hora;
    }

    public static String minutoAtual()
    {
        String minuto = "";
        dataAtual = LocalDateTime.now();

        if(dataAtual.getMinute() < 10)
        {
            minuto = "0" + Integer.toString(dataAtual.getMinute());
        }
        else
        {
            minuto = Integer.toString(dataAtual.getMinute());
        }

        return minuto;
    }

    public static String segundoAtual()
    {
        String segundo = "";
        dataAtual = LocalDateTime.now();

        if(dataAtual.getSecond() < 10)
        {
            segundo = "0" + Integer.toString(dataAtual.getSecond());
        }
        else
        {
            segundo = Integer.toString(dataAtual.getSecond());
        }

        return segundo;
    }

    public int geradorNumeroRandomico(int qtdNumero)
    {
        Random random = new Random();

        switch (qtdNumero)
        {
            case 1:
                return random.nextInt(9);

            case 2:
                return random.nextInt(99);

            case 3:
                return random.nextInt(999);

            case 4:
                return random.nextInt(9999);

            case 5:
                return random.nextInt(99999);

            case 6:
                return random.nextInt(999999);

            case 7:
                return random.nextInt(9999999);

            case 8:
                return random.nextInt(99999999);

            case 9:
                return random.nextInt(999999999);

            default:
                return random.nextInt(99999);
        }
    }

    public String geradorDataFutura(int diasAMais, int typeFormat)
    {
        String dataFinal = null, dia, mes;

        dataAtual = LocalDateTime.now().plusDays(diasAMais);
        int mesAtual = dataAtual.getMonth().getValue();

        if(dataAtual.getDayOfMonth() < 10)
        {
            dia = "0" + dataAtual.getDayOfMonth();
        }
        else
        {
            dia = Integer.toString(dataAtual.getDayOfMonth());
        }

        if(mesAtual < 10)
        {
            mes = "0" + mesAtual;
        }
        else
        {
            mes = Integer.toString(mesAtual);
        }

        switch (typeFormat)
        {
            case 1:
                dataFinal = dia + "-" + mes + "-" + dataAtual.getYear();
                break;

            case 2:
                dataFinal = dia + "/" + mes + "/" + dataAtual.getYear();
                break;
        }

        return dataFinal;
    }

    public String geradorDataHoraFutura(int diasAMais, int typeFormat)
    {
        String dataFinal = null, dia, mes;

        dataAtual = LocalDateTime.now().plusDays(diasAMais);
        int mesAtual = dataAtual.getMonth().getValue();

        if(dataAtual.getDayOfMonth() < 10)
        {
            dia = "0" + dataAtual.getDayOfMonth();
        }
        else
        {
            dia = Integer.toString(dataAtual.getDayOfMonth());
        }

        if(mesAtual < 10)
        {
            mes = "0" + mesAtual;
        }
        else
        {
            mes = Integer.toString(mesAtual);
        }

        switch (typeFormat)
        {
            case 1:
                dataFinal = dia + "-" + mes + "-" + dataAtual.getYear() + " " + dataAtual.getHour() + ":" + dataAtual.getMinute();
                break;

            case 2:
                dataFinal = dia + "/" + mes + "/" + dataAtual.getYear() + " " + dataAtual.getHour() + ":" + dataAtual.getMinute();
                break;
        }

        return dataFinal;
    }

    public String geradorDataPassado(int diasAMais, int typeFormat)
    {
        String dataFinal = null, dia, mes;

        dataAtual = LocalDateTime.now().plusDays(- diasAMais);
        int mesAtual = dataAtual.getMonth().getValue();

        if(dataAtual.getDayOfMonth() < 10)
        {
            dia = "0" + dataAtual.getDayOfMonth();
        }
        else
        {
            dia = Integer.toString(dataAtual.getDayOfMonth());
        }

        if(mesAtual < 10)
        {
            mes = "0" + mesAtual;
        }
        else
        {
            mes = Integer.toString(mesAtual);
        }

        switch (typeFormat)
        {
            case 1:
                dataFinal = dia + "-" + mes + "-" + dataAtual.getYear();
                break;

            case 2:
                dataFinal = dia + "/" + mes + "/" + dataAtual.getYear();
                break;
        }

        return dataFinal;
    }

    public String geradorDataHoraPassado(int diasAMais, int typeFormat)
    {
        String dataFinal = null, dia, mes, hora, minuto;

        dataAtual = LocalDateTime.now().plusDays(- diasAMais);
        int mesAtual = dataAtual.getMonth().getValue();

        //Dia
        if(dataAtual.getDayOfMonth() < 10)
        {
            dia = "0" + dataAtual.getDayOfMonth();
        }
        else
        {
            dia = Integer.toString(dataAtual.getDayOfMonth());
        }

        //Hora
        if(dataAtual.getHour() < 10)
        {
            hora = "0" + dataAtual.getHour();
        }
        else
        {
            hora = Integer.toString(dataAtual.getHour());
        }

        //Minuto

        if(dataAtual.getMinute() < 10)
        {
            minuto = "0" + dataAtual.getMinute();
        }
        else
        {
            minuto = Integer.toString(dataAtual.getMinute());
        }

        //Mês
        if(mesAtual < 10)
        {
            mes = "0" + mesAtual;
        }
        else
        {
            mes = Integer.toString(mesAtual);
        }

        switch (typeFormat)
        {
            case 1:
                dataFinal = dia + "-" + mes + "-" + dataAtual.getYear() + " " + hora + ":" + minuto;
                break;

            case 2:
                dataFinal = dia + "/" + mes + "/" + dataAtual.getYear() + " " + hora + ":" + minuto;
                break;
        }

        return dataFinal;
    }

    public String anoPassado()
    {
        dataAtual = LocalDateTime.now();
        return Integer.toString(dataAtual.plusYears(- 1).getYear());
    }

    //Método de gerar cpf dinamico
    public String GeradorCpf()
    {
        String iniciais = "";
        Integer numero;

        for (int i = 0; i < 9; i++)
        {
            numero = new Integer((int) (Math.random() * 9));

            iniciais += numero.toString();

        }
        return iniciais + calcDigVerif(iniciais);
    }

    public String createCNPJ()
    {
        int digito1 = 0, digito2 = 0, resto = 0;
        String  nDigResult;
        String numerosContatenados;
        String numeroGerado;

        Random numeroAleatorio = new Random();


        //numeros gerados
        int n1 = numeroAleatorio.nextInt(10);
        int n2 = numeroAleatorio.nextInt(10);
        int n3 = numeroAleatorio.nextInt(10);
        int n4 = numeroAleatorio.nextInt(10);
        int n5 = numeroAleatorio.nextInt(10);
        int n6 = numeroAleatorio.nextInt(10);
        int n7 = numeroAleatorio.nextInt(10);
        int n8 = numeroAleatorio.nextInt(10);
        int n9 = numeroAleatorio.nextInt(10);
        int n10 = numeroAleatorio.nextInt(10);
        int n11 = numeroAleatorio.nextInt(10);
        int n12 = numeroAleatorio.nextInt(10);


        int soma = n12*2 + n11*3 + n10*4 + n9*5 + n8*6 + n7*7 + n6*8 + n5*9 + n4*2 + n3*3 + n2*4 + n1*5;

        int valor = (soma / 11)*11;

        digito1 = soma-valor;

        //Primeiro resto da divis㯠por 11.
        resto = (digito1 % 11);

        if(digito1 < 2){
            digito1 = 0;
        }
        else {
            digito1 = 11-resto;
        }

        int soma2 =  digito1*2 + n12*3 + n11*4 + n10*5 + n9*6 + n8*7 + n7*8 + n6*9 + n5*2 + n4*3 + n3*4 + n2*5 + n1*6 ;

        int valor2 = (soma2 / 11)*11;

        digito2 = soma2-valor2;

        //Primeiro resto da divis㯠por 11.
        resto = (digito2 % 11);

        if(digito2 < 2){
            digito2 = 0;
        }
        else {
            digito2 = 11-resto;
        }

        //Conctenando os numeros
        //numerosContatenados = String.valueOf(n1) + String.valueOf(n2) +"."+ String.valueOf(n3) + String.valueOf(n4) +
        //                      String.valueOf(n5) +"."+ String.valueOf(n6) + String.valueOf(n7) +String.valueOf(n8)+"/"+
        //                      String.valueOf(n9) + String.valueOf(n10) + String.valueOf(n11) +
        //						String.valueOf(n12)+"-";

        numerosContatenados = String.valueOf(n1) + String.valueOf(n2) + String.valueOf(n3) + String.valueOf(n4) +
                String.valueOf(n5) + String.valueOf(n6) + String.valueOf(n7) +String.valueOf(n8) +
                String.valueOf(n9) + String.valueOf(n10) + String.valueOf(n11) +
                String.valueOf(n12);
        //Concatenando o primeiro resto com o segundo.
        nDigResult = String.valueOf(digito1) + String.valueOf(digito2);

        numeroGerado = numerosContatenados+nDigResult;

        return numeroGerado;
    }

    //Método de verificação e validação dos dois ultimos digitos do cpf
    private String calcDigVerif(String num)
    {
        Integer primDig, segDig;

        int soma = 0, peso = 10;

        for (int i = 0; i < num.length(); i++)

            soma += Integer.parseInt(num.substring(i, i + 1)) * peso--;

        if (soma % 11 == 0 | soma % 11 == 1)

            primDig = new Integer(0);

        else

            primDig = new Integer(11 - (soma % 11));

        soma = 0;

        peso = 11;

        for (int i = 0; i < num.length(); i++)

            soma += Integer.parseInt(num.substring(i, i + 1)) * peso--;

        soma += primDig.intValue() * 2;

        if (soma % 11 == 0 | soma % 11 == 1)

            segDig = new Integer(0);

        else

            segDig = new Integer(11 - (soma % 11));

        return primDig.toString() + segDig.toString();
    }

    public String createPhoneNumber()
    {
        String phoneNumber = "";

        for (int i = 0; i < 7; i++) {
            int j = (int) (Math.random() * 10);
            phoneNumber += String.valueOf(j);
        }
        return "1198" + phoneNumber;
    }

    public static String createEmailMailinator()
    {
        return GeradorNome().toLowerCase() + "@mailinator.com";
    }

    public static String GeradorNome()
    {
        return RandomLetras(10) + " " + RandomLetras(10);
    }

    public static Integer RandomNumber(Integer site)
    {
        return random.nextInt(site);
    }

    public static String GeneratorPasswordAleatory(Integer amountCaracteres) { return RandomLetrasNumeros(amountCaracteres);}

    private static String RandomLetras(Integer quantidadeLetras)
    {
        char[] letras = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
        sb = new StringBuffer();
        for (int i = 1; i <= quantidadeLetras; i++)
        {
            int ch = random.nextInt(letras.length);
            sb.append (letras [ch]);
        }

        return sb.toString();
    }

    private static String RandomLetrasNumeros(Integer quantidadeLetras)
    {
        char[] letras = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray();
        sb = new StringBuffer();
        for (int i = 1; i <= quantidadeLetras; i++)
        {
            int ch = random.nextInt(letras.length);
            sb.append (letras [ch]);
        }

        return sb.toString();
    }
}
