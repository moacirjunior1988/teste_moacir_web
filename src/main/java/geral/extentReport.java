package geral;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

public class extentReport extends utils
{
    public static ExtentTest test;
    private static ExtentHtmlReporter htmlReporter;
    public static ExtentReports extentReporter;
    private static String caminhoParaSalvarEvidenciasEReportHtml = Paths.get(System.getProperty("user.dir").toString(), "extentReports").toString();

    //Mtodo de criao do arquivo de report em html
    public static void criarArquivoHTML()
    {
        utils.validationPath(caminhoParaSalvarEvidenciasEReportHtml);
        htmlReporter = new ExtentHtmlReporter(Paths.get(caminhoParaSalvarEvidenciasEReportHtml + "/htmlReport.html").toString());
        htmlReporter.config().setChartVisibilityOnOpen(true);
        htmlReporter.config().setDocumentTitle("Teste Moacir Web");
        htmlReporter.config().setReportName("Teste.Moacir.Web.TestReport");
        htmlReporter.config().setTestViewChartLocation(ChartLocation.TOP);
        htmlReporter.config().setTheme(Theme.STANDARD);
        htmlReporter.config().setTimeStampFormat("dd/MM/yyyy HH:mm:ss");

        extentReporter = new ExtentReports();
        extentReporter.attachReporter(htmlReporter);
    }

    //Geradro de evidncia da execuo
    private static String capture() throws IOException
    {
        String nomeprint = 	anoAtual() + "_" + mesAtual() + "_" + diaAtual() + "_" +
                horaAtual() + "_" + minutoAtual() + "_" + segundoAtual() + ".png";

        File scrFile = ((TakesScreenshot) seleniumActions.webDriver).getScreenshotAs(OutputType.FILE);
        File imagemDestino = new File(Paths.get(pathSalvarEvidencia, nomeprint).toString());
        String errflpath = imagemDestino.getAbsolutePath();
        FileUtils. copyFile(scrFile, imagemDestino);
        nomeprint = null;

        return errflpath;
    }

    //Metodo de escrever no arquivo report com o status de sucesso.
    public static void SalvarLogSucesso(String text)
    {
        test.pass(text);
    }

    //Metodo de escrever no arquivo report com o status de falha.
    public void SalvarLogFalha(String text)
    {
        test.fail(text);
    }

    //Metodo de escrever no arquivo report com o status de informativo.
    public void SalvarLogInformacoes(String text)
    {
        test.info(text);
    }

    //Metodo de escrever no arquivo report com o status de sucesso com evidncia.
    public static void SalvarLogScreenShotSucesso(String text)
    {
        try {
            test.pass(test.addScreenCaptureFromPath(capture()) + text);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    //Metodo de escrever no arquivo report com o status de falha com evidncia.
    public static void SalvarLogScreenShotFalha(String text)
    {
        try {
            test.fail(test.addScreenCaptureFromPath(capture()) + text);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    //Metodo de escrever no arquivo report com o status de informativo com evidncia.
    public static void SalvarLogScreenShotInformacoes(String text)
    {
        try {
            test.info(test.addScreenCaptureFromPath(capture()) + text);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
