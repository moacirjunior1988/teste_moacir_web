package geral;

import enums.byElements;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class seleniumActions
{
    private static By by;
    public static WebDriver webDriver;
    public static Actions action;
    public static WebDriverWait wait;

    //Methods Selenium
    public static boolean WaiElement(By by)
    {
        int count = 0;
        boolean displayed = false;

        while(!displayed)
        {
            if(webDriver.findElements(by).size() > 0) displayed = true;

            utils.Sleep(1000);

            count++;

            if(count > 10) break;
        }

        return displayed;
    }

    public static WebElement GetElement(byElements byElements, String toFind)
    {
        switch (byElements)
        {
            case ClassName : //ClassName
                by = By.className(toFind);
                break;

            case CssSelector: //CssSelector
                by = By.cssSelector(toFind);
                break;

            case Id: //Id
                by = By.id(toFind);
                break;

            case LinkText: //LinkText
                by = By.linkText(toFind);
                break;

            case Name: //Name
                by = By.name(toFind);
                break;

            case PartialLinkText: //PartialLinkText
                by = By.partialLinkText(toFind);
                break;

            case TagName: //TagName
                by = By.tagName(toFind);
                break;

            case XPath: //XPath
                by = By.xpath(toFind);
                break;
        }

        if(WaiElement(by)) return webDriver.findElement(by);
        else return webDriver.findElement(by);
    }

    public static List<WebElement> GetElements(byElements byElements, String toFind)
    {
        switch (byElements)
        {
            case ClassName : //ClassName
                by = By.className(toFind);
                break;

            case CssSelector: //CssSelector
                by = By.cssSelector(toFind);
                break;

            case Id: //Id
                by = By.id(toFind);
                break;

            case LinkText: //LinkText
                by = By.linkText(toFind);
                break;

            case Name: //Name
                by = By.name(toFind);
                break;

            case PartialLinkText: //PartialLinkText
                by = By.partialLinkText(toFind);
                break;

            case TagName: //TagName
                by = By.tagName(toFind);
                break;

            case XPath: //XPath
                by = By.xpath(toFind);
                break;
        }

        if(WaiElement(by)) return webDriver.findElements(by);
        else return null;
    }

    public static void Sendkeys(WebElement element, String text)
    {
        element.clear();
        element.sendKeys(text);
        extentReport.SalvarLogScreenShotSucesso("Escrevendo no campo " + element.toString());
    }

    public static void SendkeysValue(WebElement element, String text)
    {
        ClearValue(element);
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].value = '" + text + "';", element);
        extentReport.SalvarLogScreenShotSucesso("Escrevendo no campo " + element.toString());
    }

    public static void Clear(WebElement element)
    {
        element.clear();
    }

    public static void ClearValue(WebElement element)
    {
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].value = '';", element);
    }

    public static void Clicks(WebElement element)
    {
        element.click();
        extentReport.SalvarLogScreenShotSucesso("Clicando no campo " + element.toString());
    }

    public static void DoubleClicks(WebElement element)
    {
        element.click();
        element.click();
        extentReport.SalvarLogScreenShotSucesso("Clicando no campo " + element.toString());
    }

    public static void ClicksJS(WebElement element)
    {
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click()", element);
        extentReport.SalvarLogScreenShotSucesso("Clicando no campo " + element.toString());
    }

    public static void ClickHold(WebElement element)
    {
        Actions action = new Actions(webDriver);
        action.clickAndHold(element).build().perform();
        //you need to release the control from the test
        //actions.MoveToElement(element).Release();
    }

    public static void ComboBoxSelect(WebElement element, String text)
    {
        Select selectElement = new Select(element);
        selectElement.selectByVisibleText(text);
        extentReport.SalvarLogScreenShotSucesso("Selecionando no campo " + element.toString());
    }

    public static void ComboBoxSelectValue(WebElement element, String text)
    {
        Select selectElement = new Select(element);
        selectElement.selectByValue(text);
        extentReport.SalvarLogScreenShotSucesso("Selecionando no campo " + element.toString());
    }

    public static String Text(WebElement element)
    {
        if(!element.getText().equals("")) return element.getText();
        else return "";
    }

    public static Boolean ValidacoesDeElementos(WebElement element)
    {
        if(element.isEnabled()) return true;
        else return false;
    }

    public static void Scrool(int x, int y)
    {
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("scroll("+ x +"," + y + ");");
    }

    public static void SelecionaIFrame(WebElement element)
    {
        webDriver.switchTo().frame(element);
    }

    public static String getUrl()
    {
        return webDriver.getCurrentUrl();
    }

    public static void setUrl(String url)
    {
        webDriver.navigate().to(testConfiguration.url + url);
    }
}
